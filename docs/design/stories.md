Revengate Stories
=================

Long narrations and short stories set in the Revengate universe. 


## The Audition
The blade was moving dangerously fast around the young man's Adam's apple. He was fighting hard to suppress his urge to swallow.

*Psssscht!*

The loud hissing came every minute or so but it didn't seem to have any effect on the swift strokes of the barber's blade.

"How's the job hunting?" the barber asked.

"Not good, Michel," the young man on the chair replied. "No one is hiring architects anymore. It's like they aren't making building anymore. All they are looking for are sailors and engineers, probably to build more of those dragons like they have next door."

"That fierce beast is a rotopress," Michel replied. "Instead of going up and down, it goes round and round. It prints pages by rolling the types over them. It's quite the sight."

Michel worked in silence for a several minutes, then he switched to a smaller blade for the delicate short strokes under the young man's nose.

"Have you tried growing a mustache?" Michel asked. "I can shape it for you and it would make you look more experienced." Michel stepped back to consider his work. "But it might also give thieves the impression that you've been successful in life. Did you say you can fight?"

"Don't worry about that, Michel. My father saw that I learned how to defend myself." There was not point in hiding the truth. "He was a very assertive man and more often than not, it's him that I had to wrestle down." 

Michel the barber removed the remnants of shaving soap with a wet towel, then rubbed some fragrant lotion between his hands. The young man braces for the sting, but he was sank back down in the chair when a blanket of comfort enveloped his face.

"This smells good!" The young man exclaimed. "Quite exotic!"

"This is sandal wood. Traders from Morocco have it at the market from time to time, but the only reliable supply is with the smugglers" Michel passed the young man a large mirror, almost 10cm across. "You'd probably get lost down there. They say those dark passages are as twisty as Pinot vines."

The barber was obviously referring to the network of corridors built under Lyon over the years by the silk workers.

"The traboules?" the young man replied. "I've seen many plans in school. I don't think they are the maze that people claim they are. They mostly run East-West and their is a method to their madness. Structural walls for example are very obvious. Those never twist." 

A faint smile slowly grew under the barber's perfectly curved mustache. 

"Today's shave is on the house, young man, to help you settle in your new life. Now please do me a favour and exit from the back. You'll find the door on the other side of that broom closet. If you bump into my friend Nadège on the way out, tell her that I sent you and that you are looking for work."

Puzzled, the young man followed the barber's directions. The closet door lead to a long hallway, which in turn lead to an orate green door. Beyond the door was a café with less than a dozen guests. No one seemed to pay attention to the young man when he sat at the bar.


## The Initiation

There were a dozen students sitting around the table, all bent over large leather bound books. They were all in their early twenties and judging by their attires, they came from all walks of life.

An older man entered the room. "Neophytes," he called, "Brother de Lévi is here for your aptitude test."

Brother de Lévi followed. We was in his early forties and was wearing a dark robe. On his shoulder was a leather satchel and in his hand was a long cane with an intricate large pommel. He placed both on the table before taking a seat. The older man who had introduced him retreated to a corner of the room.

De Lévi studied the room while the students exchanged excited glances and closed their books. 

"I would like you to close your eyes," de Lévi called, "and thing back to the brightest fire you've ever seen. For some of you, it is going to be a bonfire at the solstice festival, some of you will think of the coal box on a great steam engine, and maybe a lucky few fill remember the roaring furnace of a steal foundry."

De Lévi retrieved a notebook, a fountain pen, and a glass rod from his satchel.

"Remember the color of the fire," he continued, "remember the smell, and its heat on your face. Bring the fire in front of you and hold it between your hands."

Without opening their eyes, the students slowly moved their arms. Some held their arms wide apart, some had their hand cupped in a tight ball, and there was everything in between.

"Breath deeper now," de Lévi called, "and fan those flames. Make them grow brighter... hotter... stronger..."

The faces of the students came alive with their effort. Some brows became wrinkly from being pushed together, some mouths frowned while some opened to eased a laboured breathing. De Lévi scribbled some notes.

"Brighter... hotter... stronger..." he reminded them.

The only sounds in the room were deep breathing and a faint ticking that came from de Lévi's cane. It was not as fast as the ticking of a pocked watch and not as slow as that of a pendulum clock, about halfway in between. A trained ear could have recognized the distinctive two-tone clicking of a lever escapement, but that detail was missed since everyone in the room had their attention somewhere else.

"Brighter... hotter... stronger... Bring the flames right here in front of you." 

De Lévi took his glass rod and stood up. He walked next to a student and placed the bulbous end of the rod between the students hands. He held it there for a moment.

"Brighter... hotter... stronger..." 

He moved around the table, repeating the process and gently separating the hands that were help too close together. When he was done, he went back to his seat.

"You can relax now," de Lévi told them, "but before you open your eyes, you must quench your fire. You can make it starve or drown it in water. It's all up to you."

He jotted some more notes while the students opened their eyes one by one. They exchanged puzzled looks and smiles in silence while de Lévi was scribbling.

"I'm going to recommend four of you for the study of channeling." He told them. "The rest of you will get to decide between potions making, ancient languages, and the crafting of arcane devices. Sister, what is your name?" He pointed his pen at one student who immediately started blushing.

"Jeanne." She responded.

"Jeanne," de Lévi continued, "you have a boiler inside of you that has more steam than five train engines together. I will be mentoring you personally once a month." He closed his notebook and caped his pen. "That is all for me today and I hope to see you all at the next full moon ritual."

"Brother de Lévi," a student sheepishly called. "We were told that you might give us a demonstration."

De Lévi turned around towards the older man in the corner of the room who met his gaze with a nod and a smile.

"I see." De Lévi tucked his belongings back into his satchel and grabbed his cane while he rose. "Channeling is entirely possible with the mind alone," he explained, "but we now know that it is also greatly facilitated by the use of the appropriate device."

He rotated a disc at the base of the pommel of his cane. Everyone could plainly see through the elegant cuts in the pommel that something colorful was moving inside. He closed his eyes and lifted the cane high above his head.

They immediately felt the chill.

It took no time for their breath to start fogging up. It was more than just a feeling, the whole room was now as cold as a winter night. The students exchanged looks of surprise and euphoria as they started to shiver.

With his eyes still closed, de Lévi slowly brought down his cane. When its base touched the floor, the wind started. It was a gentle breeze at first, but it grew stronger with every rising and falling of de Lévi's chest. Soon enough, pages and light objects were flying across the room and the students were crouching to avoid them.

De Lévi lifted his cane then it all stopped.

He opened his eyes and started rotating the disc at the base of the pommel. 

"Brother de Lévi," a student called, "that was most eloquent. Will we learn this spell?"

"Yes," de Lévi replied with a smile. "If you study study hard, ten years from now you will master this little party trick and a whole lot more. If you study hard."

De Lévi grabbed his satchel and gave a modest bow.

"That is all for me today and I hope to see you all at the next full moon ritual. Welcome to the Blue Circle Lodge!"
